# Robot Technical  Test

How to run this project

clone to your local machine
Clone with SSH 
 ```
git@gitlab.com:open-pm/robot-technical-test.git
```
Clone with HTTPS
```
 git@gitlab.com:open-pm/robot-technical-test.git
```

If you are using python virtual environment - you virtualize the project directory

install requirements pip install -r requirements.txt

docs/ contain documentation of already run test
    
1. log.html
2. report.html - contains the report
3. index.html - contains test background. Kindly refer to this file for more information on this project
    
code/ contains python and robot code. 

To run all suits 
```
 robot code/
 ```
 
To run individual suits
```
    robot gherkin-boundary-values-analysis-test-cases.robot
    robot gherkin-equivalence-test-cases.robot
    robot key-word-exploratory-test.robot
```

    



