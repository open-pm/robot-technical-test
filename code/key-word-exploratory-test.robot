*** Settings ***
Documentation     Error guessing/exploratory testing.
...
...               This test has a workflow is implemented using key-word
...               test case creation. We used random values that are likely to
...               Throw an error to the application
...               Such as [random numbers lower than -100 and bigger than 100 50, -50, 0, 13]

Library           TestLibrary.py


*** Test Cases ***
Validate values lower than -100
    Generate random lower number
    Check validity of random number
    result should be text  System error

Validate values larger than 100
    Generate random larger number
    check validity of random number
    result should be text  System error

Validate Positive values within and inclusive of limits 50
    Check validity of num  50
    Valid will be equal to  10/50

Validate Negative values within and inclusive of limits -50
    Check validity of num  -50
    Valid will be equal to  10/-50

Validate values within and inclusive of limits 0
    Check validity of num  0
    Valid will be equal to  10/0

Validate mid number 13
    invalid number  13
    result should be text  System error
