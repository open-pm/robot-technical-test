from testcode import Testcode

import random
class TestLibrary(object):

    """Test library for testing *Testcode* functional logic.

    """

    def __init__(self):
        """
        static variables to use in the class
        _test_object is an object of the class(AUT)
        _output for keeping track of the output returned
        from object {System error, 10/num}
        """
        self._test_object = Testcode()
        self._output = ''
        self._input = ''

    def start_test(self, _min_number):
        """
        Standard class for key-word Given in case where
        no action is required to happen.

        Examples

        | Start test | _min_number | print(_min_number |

        :param _min_number: test data
        :return: None
        """
        print(_min_number)

    def generate_random_lower_number(self):
        """
        This function generates random values lower than -100
        for testing EP, in this case -200 is arbitrary

        Examples:
        | Generate random lower number  |  -101 |
        | Generate random lower number  |  -111 |

        :return: None
        """
        self._input = random.randint(-200, -101)

    def generate_random_larger_number(self):
        """
        This function generates random values larger than 100
        for testing EP. 200 is arbitrary

        Examples
        | Generate random larger number | 199 |
        | Generate random larger number | 200 |

        :return:
        """
        self._input = random.randint(101, 200)

    def generate_valid_random_number_not_13(self):
        """
        Generate valid random number between -100 and 100 both limits
        inclusive. This is recursive function that generates a
        random number not 13

        Examples
        | Generate valid random number not 13 | 14 |
        | Generate valid random number not 13 | 0 |


        :return:
        """
        self._input = random.randint(-100, 100)
        if self._input == 13:
            self.generate_valid_random_number_not_13()

    def check_validity_of_random_number(self):
        """
        Checks the validity of the number whether it
        will break the function or not. It catches
        exception raised in _test_objects.output(num)
        it should used when test for invalid random values

        Examples
        | Check validity of random number | -101 | System error |
        | Check validity of random number | 101 | System error |

        :return:
        """
        try:
            self._output = self._test_object.output(int(self._input))
        except Exception as e:
            self._output = e

    def invalid_number (self, _lower_than_upper_lower_bound):
        """ Used to check for invalid test data in BVA. Should
         always System error

         Examples
        | Invalid number | 13 | System error |
        | Invalid number | -101 | System error |

        :return:
        """
        try:
            self._output = self._test_object.output(int(_lower_than_upper_lower_bound))
        except Exception as e:
            self._output = e

    def valid_number (self, _lower_than_upper_lower_bound):
        """ Used to check for valid test data in BVA. Should
        always System error 10/(_lower_than_upper_lower_bound)

        Examples
       | Valid number | 90 | 0.1111 |
       | Vvalid number | -100 | -0.1 |

       :return:
       """
        try:
            self._output = self._test_object.output(int(_lower_than_upper_lower_bound))
        except Exception as e:
            self._output = e

    def check_validity_of_num(self, _valid_lower_min):
        """
        Checks the validity of the number whether it
        will break the function or not. it used when test valid numbers in EP
       It should always return 10/_valid_lower_min

        Examples
        | Check validity of num | 90 | 0.1111|
        | Check validity of num | -100 | -0.1 |

        :return:
        """

        try:
            self._output = self._test_object.output(int(_valid_lower_min))
        except Exception as e:
            self._output = e

    def result_should_be(self, expected):
        """This function asserts the expected and _outout
        output based on the test data. In this case valid test using when using BVA

        Examples
        | Result should be | 90 | 10/90 == expected |
        | Result should be | 100 | 10/100 == expected |

        :return:
        """
        if float(self._output) != float(expected):
            raise AssertionError('%s != %s' % (self._output, expected))

    def result_should_be_text(self, expected):
        """This function asserts the expected and _outout
        output based on the test data. In this case invalid test using when using BVA

        Examples
        | Result should be text | -199 | System error == expected |
        | Result should be text | 1000 | System error == expected |

        :return:
        """

        if str(self._output) != str(expected):
            raise AssertionError('%s != %s' % (self._output, expected))

    def valid_will_be_equal_to(self, expected):
        """This function asserts the expected and _outout
        output based on the test data. In this case invalid test using when using EP

        Examples
        | Valid will  be equal to | -99 | 10/-99 == expected |
        | Valid will  be equal to | 100 | 10/100 == expected |

        :return:
        """
        numerator, denominator = expected.split("/")
        valid_result = int(numerator)/int(denominator)

        if float(self._output) != valid_result:
            raise AssertionError('%s != %s' % (self._output, expected))

    def equivalence_result_validation(self):
        """This function asserts the expected and _outout
        output based on the test data. In this case invalid test using when using EP

        Examples
        | Equivalence result validation | -199 | System error == expected |
        | Equivalence result validation | 1001 | System error == expected |

        :return:
        """
        expected = int(self._input)/10
        if self._output != expected:
            raise AssertionError('%s != %s' % (self._output, expected))

