*** Settings ***
Documentation     Boundary Values Analysis test case using the gherkin syntax.
...
...               There are wo functions derived from the given python function
...               The lower boundary function which is -100 to 12 and upper boundary
...               which is from 14 to 100 all limits inclusive.
...               Invalid ranges are: < -100, > 100 and 13
...               Valid ranges are -100 to 100 where value != 13
...               Test cases starting with invalid are values selected from the invlaid ranges
...               And th valid ones those selected from valid ranges from both functions
Library           TestLibrary.py

*** Test Cases ***

Invalid Minimum Boundary Values
    Given boundary minimum number -100
    When invalid minimum boundary number derived -101
    Then valid result System error

Invalid Lower Maximum Boundary Values
    Given boundary lower maximum number 12
    When invalid lower maximum boundary number derived 13
    Then result is System error

Invalid Upper Minimum Boundary Values
    Given boundary upper minimum number 14
    When invalid minimum boundary number derived 13
    Then upper validation is System error

Invalid Maximum Boundary Values
    Given boundary upper maximum number 100
    When invalid maximum boundary number derived 101
    Then max boundary validation is System error

Valid lower boundary
    Given lower boundary minimum number -100
    When check the validity of the number -100
    Then boundary minimum validation is 10/-100

Valid lower max number
    Given valid minimum upper num 11
    When check validity of the valid min num number 11
    Then valid lower max is 10/11

Valid lower upper boundary
    Given lower boundary number 12
    When check validity of the lower boundary number 12
    Then result of lower upper boundary "10/12"

Valid upper min Number
    Given boundary upper minimun number 14
    When check validity of the upper minimum number 14
    Then result of upper minim is 10/14

Valid upper lower number
    Given minimum upper number 15
    When check validity of the upper lower number 15
    Then result of upper lower number is 10/15

Valid upper max number
    Given valid maximum upper number 99
    When check validity of the upper max number 99
    Then result of upper max is 10/99

Valid upper boundary number
    Given boundary maximum number 100
    When check validity of the upper boundary number 100
    Then result of upper boundary is 10/100

Validate midnumer 13
    Given a midnumber 13
    When check validity of midpoint number 13
    Then midpoint number validation is System error

Validate valid number 0
    Given a valid number 0
    When validity of zero is checked 0
    Then result of zero validity is 10/0


*** Keywords ***
Boundary minimum number  ${lower_min_number}
    start test  ${lower_min_number}
Invalid minimum boundary number derived ${lower_than_lower_min_number}
    invalid number  ${lower_than_lower_min_number}
Valid result ${result}
    Result should be text  ${result}

Boundary lower maximum number ${lower_max_num}
    start test  ${lower_max_num}
Invalid lower maximum boundary number derived ${larger_than_lower_upper_bound_number}
    invalid number  ${larger_than_lower_upper_bound_number}
Result is ${result}
    result should be text  ${result}

Boundary upper minimum number ${upper_min_number}
    start test  ${upper_min_number}
Invalid upper minimum boundary number derived ${lower_than_upper_min_number}
    invalid number  ${lower_than_upper_min_number}
Upper validation is ${result}
    result should be text  ${result}

Boundary upper maximum number ${_max_num}
    start test  ${_max_num}
Invalid maximum boundary number derived ${larger_than_upper_bound_number}
    invalid number  ${larger_than_upper_bound_number}
Max boundary validation is ${result}
    Result should be text  ${result}

Given a midnumber ${midpoint}
    start test  ${midpoint}
Check validity of midpoint number ${midpoint}
    invalid number  ${midpoint}
Midpoint number validation is ${result}
    Result should be text  ${result}



Lower boundary minimum number ${valid_lower_boundary}
    start test  ${valid_lower_boundary}
Check the validity of the number ${valid_lower_boundary}
    valid number  ${valid_lower_boundary}
Boundary minimum validation is ${result}
    valid will be equal to  ${result}

Valid minimum upper num ${_valid_lower_max}
    start test  ${_valid_lower_max}
Check validity of the valid min num number ${_valid_lower_max}
    valid number  ${_valid_lower_max}
Valid lower max is ${expression}
    valid will be equal to   ${expression}

Lower boundary number ${_lower_upper_boundary}
    start test    ${_lower_upper_boundary}
Check validity of the lower boundary number ${_lower_upper_boundary}
    valid number     ${_lower_upper_boundary}
Result of lower upper boundary "${result}"
    valid will be equal to     ${result}

Boundary upper minimun number ${_upper_min_num}
    start test  ${_upper_min_num}
Check validity of the upper minimum number ${_upper_min_num}
    valid number  ${_upper_min_num}
Result of upper minim is ${result}
    valid will be equal to  ${result}

Minimum upper number ${_valid_upper_lower_num}
    start test  ${_valid_upper_lower_num}
Check validity of the upper lower number ${_valid_upper_lower_num}
    valid number  ${_valid_upper_lower_num}
Result of upper lower number is ${result}
    valid will be equal to  ${result}

Valid maximum upper number ${_valid_upper_max_num}
    start test  ${_valid_upper_max_num}
Check validity of the upper max number ${_valid_upper_max_num}
    valid number  ${_valid_upper_max_num}
Result of upper max is ${result}
    valid will be equal to  ${result}

Boundary maximum number ${_max_boundary_num}
    start test  ${_max_boundary_num}
Check validity of the upper boundary number ${_max_boundary_num}
    valid number  ${_max_boundary_num}
result of upper boundary is ${result}
    valid will be equal to  ${result}

A valid number ${zero}
    start test  ${zero}
Validity of zero is checked ${zero}
    valid number  ${zero}
Result of zero validity is ${result}
    valid will be equal to  ${result}
