*** Settings ***
Documentation     Equivalence Partitioning test cases is implemeted using gherkin
...
...               This workflow implements EP using lower boundaries limit of <=17
...               and upper boundary limit of >=101.
...               We used numbers between 18 and 100 inclusive as valid values
...               And created another test case for value 13 and 0

Library           TestLibrary.py


*** Test Cases ***
Number from invalid class less than -100
    Given generated random number
    When validate of the number
    Then invalid error message System error

Number from invalid class greather than 100
    Given generate larger random number
    When Check validity of larger number
    Then result of larger number System error

Positive Number from valid class greather than between -100 and 100 and not 13
    Given A generated random valid number 50
    When Check validity of the valid number 50
    Then validated result is same as 10/50

Negative Number from valid class greather than between -100 and 100 and not 13
    Given A generated random valid number -50
    When Check validity of the valid number -50
    Then validated result is same as 10/-50

Zero from valid class greather than between -100 and 100 and not 13
    Given A generated random valid number 0
    When Check validity of the valid number 0
    Then validated result is same as 10/0

Test for lower limit number
    Given A lower limit number -100
    When Check validity of the valid number -100
    Then validated result is same as 10/-100

Test for upper limit number
    Given A larger limit number 100
    When Check validity of the valid number 100
    Then validated result is same as 10/100

Validate mid number 13
    Given midpoint number 13
    When check validity of the number 13
    Then result is System error


*** Keywords ***
Generated random number
    generate random lower number

Validate of the number
    check validity of random number

Invalid error message ${result}
    result should be text  ${result}

Generate larger random number
    generate random larger number

Check validity of larger number
    check validity of random number

Result of larger number ${result}
    result should be text  ${result}

A generated random valid number ${random_number}
    start test  ${random_number}

Check validity of the valid number ${random_number}
    check validity of num  ${random_number}

Validated result is same as ${result}
    valid will be equal to  ${result}

Midpoint number ${midpoint}
    start test  ${midpoint}

Check validity of the number ${larger_than_lower_upper_bound_number}
    invalid number  ${larger_than_lower_upper_bound_number}

Result is ${result}
    result should be text  ${result}

A larger limit number ${larger_limit_number}
    start test   ${larger_limit_number}

A lower limit number ${lower_limit_number}
    start test   ${lower_limit_number}
